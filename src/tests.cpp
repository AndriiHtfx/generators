/*
 * This is most common hi2 tests.
 * First: independency test
 * Second: pairs test
 * Third: partitions test
 */

#include "tests.h"
#include <utility>

const double Z2_quantile = 1.6448536;
// Z2 quantile has precision a2 = 0.05;
// we can also use next quantiles:
// Z3_quantile = 1.2815515 with following precision: a3 = 0.1
// Z1_quantile = 2.3263478 with following precision: a1 = 0.01

bool equip_test(std::vector <unsigned char> bytes)
{
    long double hi2 = 0;
    for (int i = 0; i < 0xFF; i++) {
        unsigned curr_byte_mathces = std::count(bytes.begin(), bytes.end(), i & 0xFF);
        auto n_bytes = (long double) bytes.size() / (0xFF + 1);
        hi2 += (long double) (pow(curr_byte_mathces - n_bytes, 2)) / n_bytes;
    }
    std::cout << "hi2: " << hi2 << std::endl;
    // hi2_l is sample value
    long double hi2_l = sqrt(2 * 0xFF) * Z2_quantile + 0xFF;
    std::cout << "hi2_l: " << hi2_l << std::endl;

    if (hi2 <= hi2_l) {
        return true;
    }
    return false;
}

bool intep_test(std::vector <unsigned char> bytes)
{
    std::vector <std::pair <unsigned char, unsigned char>> pairs;
    for (int i = 0; i < bytes.size() - 1; i += 2) {
        pairs.push_back(std::pair <unsigned char, unsigned char> (bytes[i], bytes[i + 1]));
    }

    std::vector <unsigned char> first_in_pair;
    std::vector <unsigned char> second_in_pair;
    for (auto pair : pairs) {
        first_in_pair.push_back(pair.first);
        second_in_pair.push_back(pair.second);
    }

    long double hi2 = 0;
    long n_matches = 0;
    for (int i = 0; i < 0xFF; i++) {
        for (int j = 0; j < 0xFF; j++) {
            std::pair <unsigned char, unsigned char> pair;
            pair.first = (unsigned char) i & 0xFF;
            pair.second = (unsigned char) j & 0xFF;
            long double curr_matches = std::count(pairs.begin(), pairs.end(), pair);
            n_matches += curr_matches;
            long double vi = std::count(first_in_pair.begin(), first_in_pair.end(), pair.first);
            long double aj = std::count(second_in_pair.begin(), second_in_pair.end(), pair.second);
            if (vi > 0 && aj > 0) {
                hi2 += (long double) (pow(curr_matches, 2) / (vi * aj));
            }
        }
    }
    hi2 = (hi2 - 1) * n_matches;

    std::cout << "hi2: " << hi2 << std::endl;
    // hi2_l is sample value
    long double hi2_l = sqrt(2 * pow(0xFF, 2)) * Z2_quantile + pow(0xFF, 2);
    std::cout << "hi2_l: " << hi2_l << std::endl;
    if (hi2 <= hi2_l) {
        return true;
    }

    return false;
}

bool hom_test(std::vector <unsigned char> bytes, unsigned partitioning_size)
{
    if (partitioning_size > bytes.size()) {
        throw "partitioning size is too long\n";
    }

    std::vector <std::vector <unsigned char>> parts;
    for (int i = 0; i < bytes.size() - partitioning_size; i += partitioning_size) {
        std::vector <unsigned char> part;
        for (int j = 0; j < partitioning_size; j++) {
            part.push_back(bytes[i + j]);
        }
        parts.push_back(part);
    }

    long double hi2 = 0;

    for (int i = 0; i < 0xFF; i++) {
        long double curr_matches = 0;
        for (std::vector <unsigned char> part : parts) {
            curr_matches += std::count(part.begin(), part.end(), i);
        }

        for (std::vector <unsigned char> part : parts) {
            long vij = std::count(part.begin(), part.end(), i);
            if (curr_matches > 0 && partitioning_size > 0) {
                hi2 += (long double) pow(vij, 2) / (curr_matches * partitioning_size);
            }
        }
    }
    hi2 = (long double) (hi2 - 1) * bytes.size();

    std::cout << "hi2: " << hi2 << std::endl;
    auto number_of_parts = (unsigned long) bytes.size() / partitioning_size;
    long double l_koef = 0xFF * (number_of_parts - 1);

    // hi2_l is sample value
    double hi2_l = sqrt(2 * l_koef) * Z2_quantile + l_koef;
    std::cout << "hi2_l: " << hi2_l << std::endl;
    if (hi2 <= hi2_l) {
        return true;
    }
    return false;
}