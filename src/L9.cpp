#include "L9.h"

L9::L9(unsigned init_state) : state(init_state) {}

int L9::shift() {
    int new_bit = ((state[0] ^ state[1]) ^ state[3]) ^state[4];
    int out = state[0];
    state >>= 1;
    state[8] = new_bit;
    return out;
}
