#include "L10.h"

L10::L10(unsigned init_state) : state(init_state) {}

int L10::shift() {
    int new_bit = state[0] ^state[3];
    int out = state[0];
    state >>= 1;
    state[9] = new_bit;
    return out;
}
