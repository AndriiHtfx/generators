#include "L89.h"

L89::L89(std::vector <unsigned char> init_bytes) {
    for (unsigned char byte: init_bytes) {
        for (int i = 0; i < 8; i++) {
            state[0] = byte & 0x1;
            byte >>= 1;
            state <<= 1;
        }
    }
}

int L89::shift() {
    int new_bit = state[50] ^ state[0];
    int out = state[88];
    state <<= 1;
    state[0] = new_bit;
    return out;
}
