#include "L11.h"

L11::L11(unsigned init_state) : state(init_state) {}

int L11::shift() {
    int new_bit = state[0] ^ state[2];
    int out = state[0];
    state >>= 1;
    state[10] = new_bit;
    return out;
}
