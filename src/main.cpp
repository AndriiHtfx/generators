#include <iostream>
#include "generators.h"
#include <fstream>
#include "generators.h"
#include "tests.h"
#include <random>
#include <chrono>
#include <bitset>

int main()
{
    // ok, let's generate 1 000 000 random bits with registry generator
    // registry length: 89
    std::vector <unsigned char> bytes = registry_gen(125000, 89);
    std::cout << "lemmer low: " << std::endl;
    equip_test(bytes);
    intep_test(bytes);
    hom_test(bytes, 20);
    bytes.clear();
    return 0;
}