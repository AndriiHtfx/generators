#include "Geffe.h"

Geffe::Geffe(unsigned init1, unsigned init2, unsigned init3) :
        reg1(init1), reg2(init2), reg3(init3) {}

int Geffe::get_bit() {
    int x = reg1.shift();
    int y = reg2.shift();
    int s = reg3.shift();

    return ((s * x) ^ ((1 ^ s) * y));
}
