#include <random>
#include <chrono>
#include <fstream>
#include <string>
#include <gmp.h>
#include <gmpxx.h>
#include "L.h"
#include "L20.h"
#include "L89.h"
#include "Geffe.h"

const int int_bits = 32;
const long max_int = (long) pow(2, int_bits) - 1;
const int bits_in_byte = 8;

std::vector <unsigned char> cxx_gen(int n_bytes)
{
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    std::uniform_int_distribution<unsigned> distribution(0, max_int);
    std::vector <unsigned char> result;
    for (int i = 0; i < n_bytes; i++) {
        result.push_back(distribution(generator) & 0xFF);
    }
    return result;
}

std::vector <unsigned char> lemmer_gen(int n_bytes, char mode)
{
    // just 3 fixed constatnts recomended to use with this generator
    const unsigned long m = (long) pow(2, 32);
    const unsigned a = (unsigned) pow(2, 16) + 1;
    const unsigned c = 119;

    unsigned x = std::chrono::system_clock::now().time_since_epoch().count();
    std::vector <unsigned char> result;
    for (int i = 0; i < n_bytes; i++) {
        if (mode == 'h') {
            result.push_back(
                    (x >> (bits_in_byte * (sizeof(unsigned) - sizeof(unsigned char)))) & 0xFF
            );
        } else if (mode == 'l') {
            result.push_back(x & 0xFF);
        } else {
            throw "unknown mode\n";
        }
        x = (a * x + c) % m;
    }
    return result;
}

std::vector <unsigned char> registry_gen(int n_bytes, int reg_size)
{
    L* registry = nullptr;
    if (reg_size == 20) {
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        registry = new L20(seed);
    } else if (reg_size == 89) {
        registry = new L89(cxx_gen(((int) 89 / bits_in_byte) + 1));
    } else {
        throw "unsupported registry size\n";
    }

    std::vector <unsigned char> result;
    for (int i = 0; i < n_bytes; i++) {
        unsigned char byte = 0x0;
        for (int j = 0; j < bits_in_byte; j++) {
            bool bit = registry->shift();
            byte |= bit;
            if (j < bits_in_byte - 1) {
                byte <<= 1;
            }
        }
        result.push_back(byte);
    }
    delete registry;
    return result;
}

std::vector <unsigned char> geffe_gen(int n_bytes)
{
    unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
    unsigned seed2 = std::chrono::system_clock::now().time_since_epoch().count();
    unsigned seed3 = std::chrono::system_clock::now().time_since_epoch().count();
    Geffe generator(seed1, seed2, seed3);
    std::vector <unsigned char> result;
    for (int i = 0; i < n_bytes; i++) {
        unsigned char byte = 0x0;
        for (int j = 0; j < bits_in_byte; j++) {
            byte |= generator.get_bit();
            if (j < bits_in_byte - 1) {
                byte <<= 1;
            }
        }
        result.push_back(byte);
    }
    return result;
}

// bad working generator. It presents here only to show that it passes 2nd and 3d tests
std::vector <unsigned char> volfram_gen(int n_bytes)
{
    unsigned r = std::chrono::system_clock::now().time_since_epoch().count();
    std::vector <unsigned char> result;
    for (int i = 0; i < n_bytes; i++) {
        unsigned char byte = 0x0;
        for (int j = 0; j < bits_in_byte; j++) {
            byte |= (r % 2);
            if (j < bits_in_byte - 1) {
                byte <<= 1;
            }
            r = (r << 1) ^ (r | (r >> 1));
        }
        result.push_back(byte);
    }
    return result;
}

// bad generator in general. It must fail all of the hi2 tests.
std::vector <unsigned char> librarian_gen(int n_bytes)
{
    // ok but could be better. e.g.: random bytes peeking instead of
    // consistent one. Anyway this is not the best generator in general.
    std::fstream fs;
    fs.open("lorem_ipsum.txt");
    if (fs.good()) {
        std::string buf;
        fs >> buf;
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        seed %= buf.length();
        std::vector<unsigned char> result;
        for (int i = 0; i < n_bytes; i++) {
            result.push_back(buf[(seed + i) % buf.length()]);
        }
        return result;
    } else {
        throw "no input file\n";
    }
}

std::vector <unsigned char> bm_gen(int n_bytes)
{
    // these numbers were recommended for this algorithm
    const char* p_str = "CEA42B987C44FA642D80AD9F51F10457690DEF10C83D0BC1BCEE12FC3B6093E3";
    const char* a_str = "5B88C41246790891C095E2878880342E88C79974303BD0400B090FE38A688356";
    const char* q_str = "675215CC3E227D3216C056CFA8F8822BB486F788641E85E0DE77097E1DB049F1";

    // 16 - number system base
    mpz_class p(p_str, 16);
    mpz_class a(a_str, 16);
    mpz_class q(q_str, 16);
    mpz_class T;

    gmp_randstate_t state;
    gmp_randinit_default(state);
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    gmp_randseed_ui(state, seed);
    mpz_urandomm(T.get_mpz_t(), state, p.get_mpz_t());

    std::vector <unsigned char> result;
    for (int i = 0; i < n_bytes; i++) {
        unsigned char byte = 0x0;
        for (int j = 0; j < bits_in_byte; j++) {
            if (T < ((p - 1) / 2)) {
                byte |= 1;
            }
            if (j < bits_in_byte - 1) {
                byte <<= 1;
            }
            mpz_powm(T.get_mpz_t(), a.get_mpz_t(), T.get_mpz_t(), p.get_mpz_t());
        }
        result.push_back(byte);
    }
    return result;
}

std::vector <unsigned char> bm_gen_bytes(int n_bytes)
{
    // these numbers were recommended for this algorithm
    const char* p_str = "CEA42B987C44FA642D80AD9F51F10457690DEF10C83D0BC1BCEE12FC3B6093E3";
    const char* a_str = "5B88C41246790891C095E2878880342E88C79974303BD0400B090FE38A688356";
    const char* q_str = "675215CC3E227D3216C056CFA8F8822BB486F788641E85E0DE77097E1DB049F1";

    // 16 - number system base
    mpz_class p(p_str, 16);
    mpz_class a(a_str, 16);
    mpz_class q(q_str, 16);

    mpz_class T = 0;
    gmp_randstate_t state;
    gmp_randinit_default(state);
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    gmp_randseed_ui(state, seed);
    mpz_urandomm(T.get_mpz_t(), state, p.get_mpz_t());

    std::vector <unsigned char> result;
    for (int i = 0; i < n_bytes; i++) {
        for (mpz_class k = 0; k < 0xFF + 1; k++) {
            if ((T > ((k * (p - 1)) / (0xFF + 1))) && (T <= (((k + 1) * (p - 1)) / (0xFF + 1)))) {
                result.push_back(k.get_ui() & 0xFF);
                break;
            }
        }
        mpz_powm(T.get_mpz_t(), a.get_mpz_t(), T.get_mpz_t(), p.get_mpz_t());
    }
    return result;
}

std::vector <unsigned char> bbs_gen(int n_bytes)
{
    // these numbers were recommended for this algorithm
    const char* p_str = "D5BBB96D30086EC484EBA3D7F9CAEB07";
    const char* q_str = "425D2B9BFDB25B9CF6C416CC6E37B59C1F";

    // 16 - number system base
    mpz_class p(p_str, 16);
    mpz_class q(q_str, 16);
    mpz_class pq = p * q;

    mpz_class r_koef = 0;
    while (r_koef < 2) {
        gmp_randstate_t state;
        gmp_randinit_default(state);
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        gmp_randseed_ui(state, seed);
        mpz_urandomm(r_koef.get_mpz_t(), state, p.get_mpz_t());
    }

    std::vector <unsigned char> result;
    const mpz_class power = 2;
    for (int i = 0; i < n_bytes; i++) {
        unsigned char byte = 0x0;
        mpz_class bit = 0;
        for (int j = 0; j < bits_in_byte; j++) {
            mpz_powm(r_koef.get_mpz_t(), r_koef.get_mpz_t(), power.get_mpz_t(), pq.get_mpz_t());
            bit = r_koef % 2;
            byte |= bit.get_ui();
            if (j < bits_in_byte - 1) {
                byte <<= 1;
            }
        }
        result.push_back(byte);
    }
    return result;
}

std::vector <unsigned char> bbs_gen_bytes(int n_bytes)
{
    // these numbers were recommended for this algorithm
    const char* p_str = "D5BBB96D30086EC484EBA3D7F9CAEB07";
    const char* q_str = "425D2B9BFDB25B9CF6C416CC6E37B59C1F";

    // 16 - number system base
    mpz_class p(p_str, 16);
    mpz_class q(q_str, 16);
    mpz_class pq = p * q;

    mpz_class r_koef = 0;
    while (r_koef < 2) {
        gmp_randstate_t state;
        gmp_randinit_default(state);
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        gmp_randseed_ui(state, seed);
        mpz_urandomm(r_koef.get_mpz_t(), state, p.get_mpz_t());
    }

    std::vector <unsigned char> result;
    const mpz_class power = 2;
    const mpz_class byte_modulo = 0xFF + 1;
    mpz_class byte = 0;
    for (int i = 0; i < n_bytes; i++) {
        mpz_powm(r_koef.get_mpz_t(), r_koef.get_mpz_t(), power.get_mpz_t(), pq.get_mpz_t());
        byte = r_koef % byte_modulo;
        result.push_back(byte.get_ui() & 0xFF);
    }
    return result;
}
