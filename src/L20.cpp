#include "L20.h"

L20::L20(unsigned init_state) : state(init_state) {}

int L20::shift() {
    int new_bit = ((state[17] ^ state[15]) ^ state[11]) ^ state[0];
    int out = state[19];
    state <<= 1;
    state[0] = new_bit;
    return out;
}
