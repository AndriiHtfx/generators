#ifndef CRYPTALAB1_L89_H
#define CRYPTALAB1_L89_H

#include <bitset>
#include <vector>
#include <L.h>

class L89 : public L {
private:
    std::bitset <89> state;

public:
    explicit L89(std::vector <unsigned char> init_bytes);

    int shift() override;
};


#endif //CRYPTALAB1_L89_H
