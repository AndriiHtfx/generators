#ifndef CRYPTALAB1_GEFFE_H
#define CRYPTALAB1_GEFFE_H

#include "L11.h"
#include "L9.h"
#include "L10.h"

class Geffe {
private:
    L11 reg1;
    L9 reg2;
    L10 reg3;

public:
    Geffe(unsigned init1, unsigned init2, unsigned init3);

    int get_bit();
};


#endif //CRYPTALAB1_GEFFE_H
