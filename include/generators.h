#include <random>
#include <chrono>
#include <iostream>

std::vector <unsigned char> cxx_gen(int n_bytes);

std::vector <unsigned char> lemmer_gen(int n_bytes, char mode);

std::vector <unsigned char> registry_gen(int n_bytes, int reg_size);

std::vector <unsigned char> geffe_gen(int n_bytes);

std::vector <unsigned char> volfram_gen(int n_bytes);

std::vector <unsigned char> librarian_gen(int n_bytes);

std::vector <unsigned char> bm_gen(int n_bytes);

std::vector <unsigned char> bm_gen_bytes(int n_bytes);

std::vector <unsigned char> bbs_gen(int n_bytes);

std::vector <unsigned char> bbs_gen_bytes(int n_bytes);