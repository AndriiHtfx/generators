#ifndef CRYPTALAB1_TESTS_H
#define CRYPTALAB1_TESTS_H

#include <vector>
#include <cmath>
#include <iostream>
#include <algorithm>

bool equip_test(std::vector <unsigned char> bytes);

bool intep_test(std::vector <unsigned char> bytes);

bool hom_test(std::vector <unsigned char> bytes, unsigned m);

#endif //CRYPTALAB1_TESTS_H
