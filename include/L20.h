#ifndef CRYPTALAB1_L20_H
#define CRYPTALAB1_L20_H

#include <bitset>

#include "L.h"

class L20 : public L {
private:
    std::bitset <20> state;

public:
    explicit L20(unsigned init_state);

    int shift() override;
};


#endif //CRYPTALAB1_L20_H
