#ifndef CRYPTALAB1_L_H
#define CRYPTALAB1_L_H


class L {
public:
    virtual int shift() = 0;
};


#endif //CRYPTALAB1_L_H
