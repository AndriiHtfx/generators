#ifndef CRYPTALAB1_L9_H
#define CRYPTALAB1_L9_H

#include <bitset>

#include "L.h"

class L9 : public L {
private:
    std::bitset <9> state;

public:
    explicit L9(unsigned init_state);

    int shift() override;
};


#endif //CRYPTALAB1_L9_H
