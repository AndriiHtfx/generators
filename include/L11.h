#ifndef CRYPTALAB1_L11_H
#define CRYPTALAB1_L11_H

#include <bitset>

#include "L.h"

class L11 : public L {
private:
    std::bitset <11> state;

public:
    explicit L11(unsigned init_state);

    int shift() override;
};


#endif //CRYPTALAB1_L11_H
