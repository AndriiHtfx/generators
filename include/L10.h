#ifndef CRYPTALAB1_L10_H
#define CRYPTALAB1_L10_H

#include <bitset>

#include "L.h"

class L10 : public L {
private:
    std::bitset <10> state;

public:
    explicit L10(unsigned init_state);

    int shift() override;
};


#endif //CRYPTALAB1_L10_H
